# coding: utf-8

"""
    Transit API

    API provided by data from transit.app  # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 3.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class InlineResponse2006(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'itineraries': 'list[ItineraryDetail]',
        'route': 'Route'
    }

    attribute_map = {
        'itineraries': 'itineraries',
        'route': 'route'
    }

    def __init__(self, itineraries=None, route=None):  # noqa: E501
        """InlineResponse2006 - a model defined in Swagger"""  # noqa: E501
        self._itineraries = None
        self._route = None
        self.discriminator = None
        self.itineraries = itineraries
        self.route = route

    @property
    def itineraries(self):
        """Gets the itineraries of this InlineResponse2006.  # noqa: E501


        :return: The itineraries of this InlineResponse2006.  # noqa: E501
        :rtype: list[ItineraryDetail]
        """
        return self._itineraries

    @itineraries.setter
    def itineraries(self, itineraries):
        """Sets the itineraries of this InlineResponse2006.


        :param itineraries: The itineraries of this InlineResponse2006.  # noqa: E501
        :type: list[ItineraryDetail]
        """
        if itineraries is None:
            raise ValueError("Invalid value for `itineraries`, must not be `None`")  # noqa: E501

        self._itineraries = itineraries

    @property
    def route(self):
        """Gets the route of this InlineResponse2006.  # noqa: E501


        :return: The route of this InlineResponse2006.  # noqa: E501
        :rtype: Route
        """
        return self._route

    @route.setter
    def route(self, route):
        """Sets the route of this InlineResponse2006.


        :param route: The route of this InlineResponse2006.  # noqa: E501
        :type: Route
        """
        if route is None:
            raise ValueError("Invalid value for `route`, must not be `None`")  # noqa: E501

        self._route = route

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(InlineResponse2006, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, InlineResponse2006):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
