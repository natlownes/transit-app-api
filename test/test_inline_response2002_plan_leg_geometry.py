# coding: utf-8

"""
    Transit API

    API provided by data from transit.app  # Authentication  <!-- ReDoc-Inject: <security-definitions> -->  # noqa: E501

    OpenAPI spec version: 3.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import transit_app_api
from transit_app_api.models.inline_response2002_plan_leg_geometry import InlineResponse2002PlanLegGeometry  # noqa: E501
from transit_app_api.rest import ApiException


class TestInlineResponse2002PlanLegGeometry(unittest.TestCase):
    """InlineResponse2002PlanLegGeometry unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testInlineResponse2002PlanLegGeometry(self):
        """Test InlineResponse2002PlanLegGeometry"""
        # FIXME: construct object with mandatory attributes with example values
        # model = transit_app_api.models.inline_response2002_plan_leg_geometry.InlineResponse2002PlanLegGeometry()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
