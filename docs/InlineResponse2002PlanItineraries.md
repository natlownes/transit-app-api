# InlineResponse2002PlanItineraries

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessibility** | **str** | Whether the itinerary is accessible to riders in a wheelchair.   * &#x60;None&#x60;: Accessible trip not requested.  * &#x60;WheelchairTripsWithUnknownStops&#x60;: The trips and routes used are accessible, but the accessibility status of at least one stop in the itinerary is unknown.  * &#x60;WheelchairStrict&#x60;: The itinerary is accessible, and all necessary accessibility info was available. | 
**duration** | **float** | Duration of the itinerary, in seconds | 
**end_time** | **float** | Time at which the last leg of the itinerary ends. Represented as UNIX time, in milliseconds. | 
**legs** | [**list[InlineResponse2002PlanLegs]**](InlineResponse2002PlanLegs.md) |  | [optional] 
**start_time** | **float** | The time at which the first leg of the itinerary begins. Represented as UNIX time in milliseconds. | 
**transfers** | **float** | The number of times the user must disembark one transit vehicle and board another during this itinerary. | 
**transit_time** | **float** | Total time in seconds spent on a transit vehicle. | 
**walk_time** | **float** | Total time in seconds spent walking | 
**wheelchair_need** | **str** | Reflects the requested trip accessibility:  * &#x60;None&#x60;: Include all trips   * &#x60;WheelchairTripsWithUnknownStops&#x60;:  Require accessible trips, but include trips using stops with unknown accessibility status. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

