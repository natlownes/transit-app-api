# Route

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**global_route_id** | [**GlobalRouteId**](GlobalRouteId.md) |  | 
**itineraries** | [**list[Itinerary]**](Itinerary.md) | Itineraries will be provided in /nearby_stops but not /routes_for_network, /route_details, or /trip_details | [optional] 
**route_long_name** | **str** | [&#x60;route_long_name&#x60;](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#routestxt) from the GTFS | 
**route_short_name** | **str** | [&#x60;route_short_name&#x60;](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#routestxt) from the GTFS | 
**route_type** | **int** | Route type as defined in [GTFS](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#routestxt) | 
**route_color** | **str** | Route color as defined in [GTFS](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#routestxt) | 
**route_text_color** | **str** | Route text color as defined in [GTFS](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#routestxt) | 
**route_network_name** | **str** | The network name associated with that route. A network name is a user-presentable string that represent the branding of the route.   It is not exactly the agency as sometime the branding will be slighly different even within the same agency. For example, &#x60;network_name&#x60; will be different for local bus and commuter bus in Baltimore since they are handled as two different networks on the user&#x27;s perspective even if operated by the same agency.    Network name should be unique inside a metropolitian region but can be reused across regions. For example \&quot;MTA\&quot; will be used for both MTA in NYC and MTA in Baltimore. For an unique value, please use &#x60;route_network_id&#x60;. | [optional] 
**route_network_id** | **str** | A global unique identifier for the network. The network id will be constant on a best-effort basis and can change when network redesign and other major changes happens.   For more detail about network, please refer to &#x60;route_network_name&#x60;.  | [optional] 
**tts_long_name** | **str** | Long name for the route suitable to be used in a text-to-speech context. Not suitable for display.  | [optional] 
**tts_short_name** | **str** | Short name for the route suitable to be used in a text-to-speech context. Not suitable for display.  | [optional] 
**sorting_key** | **str** | If routes need to be sorted in the usual order they are shown in, sorting should be based of this key.   For example, Lettered MTA Subway routes are usually shown in the following order : &#x27;A&#x27;, &#x27;C&#x27;, &#x27;E&#x27;, &#x27;B&#x27;, &#x27;D&#x27;, etc. The short name is not suitable for sorting in this case so the &#x60;sorting_key&#x60; should be used.  | [optional] 
**real_time_route_id** | **str** | Route id used for real time. In most case, it is the case id coming from the original GTFS. If no real time is present, &#x60;null&#x60; is provided.   Not included in the &#x60;nearby_stops&#x60; call.  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

