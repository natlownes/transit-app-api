# transit_app_api.PublicTransportApi

All URIs are relative to *https://external.transitapp.com/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_public_available_networks**](PublicTransportApi.md#get_public_available_networks) | **GET** /public/available_networks | Get a list of available networks
[**get_public_latest_update_for_network**](PublicTransportApi.md#get_public_latest_update_for_network) | **GET** /public/latest_update_for_network | Latest data update for network
[**get_public_route_details**](PublicTransportApi.md#get_public_route_details) | **GET** /public/route_details | Get detail for a route
[**get_public_routes_for_network**](PublicTransportApi.md#get_public_routes_for_network) | **GET** /public/routes_for_network | Routes for a given network
[**get_public_search_stops**](PublicTransportApi.md#get_public_search_stops) | **GET** /public/search_stops | Find transit stops by search term
[**get_public_stops_for_network**](PublicTransportApi.md#get_public_stops_for_network) | **GET** /public/stops_for_network | Stops for a given network
[**get_v3_public_trip_details**](PublicTransportApi.md#get_v3_public_trip_details) | **GET** /v3/public/trip_details | Get details for a trip.
[**nearby_stops**](PublicTransportApi.md#nearby_stops) | **GET** /public/nearby_stops | Get transit stops which are near a location
[**otp**](PublicTransportApi.md#otp) | **GET** /otp/plan | Plan a trip from origin to destination
[**stop_departures**](PublicTransportApi.md#stop_departures) | **GET** /public/stop_departures | Get upcoming departures for all routes serving a stop

# **get_public_available_networks**
> InlineResponse2004 get_public_available_networks(lat=lat, lon=lon, include_all_networks=include_all_networks, accept_language=accept_language)

Get a list of available networks

List of networks available in the entire Transit system. For more information about networks, refer to the /public/stop_departures endpoint and the 'route_network_name'. Optionnaly, a lat/lon can be passed to filter networks to only the ones that support that coordinate. 

### Example
```python
from __future__ import print_function
import time
import transit_app_api
from transit_app_api.rest import ApiException
from pprint import pprint

# Configure API key authorization: apiKey
configuration = transit_app_api.Configuration()
configuration.api_key['apiKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# create an instance of the API class
api_instance = transit_app_api.PublicTransportApi(transit_app_api.ApiClient(configuration))
lat = 1.2 # float | Optional latitude, if provided in addition to `lon` the network returned will only include network serving that location.  (optional)
lon = 1.2 # float | Optional longitude, if provided in addition to `lat` the network returned will only include network serving that location.  (optional)
include_all_networks = false # bool | Include additional supported networks that are filtered out by default. For example, these may include school busses and previews of upcoming agency network redesigns. (optional) (default to false)
accept_language = 'accept_language_example' # str | Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  (optional)

try:
    # Get a list of available networks
    api_response = api_instance.get_public_available_networks(lat=lat, lon=lon, include_all_networks=include_all_networks, accept_language=accept_language)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicTransportApi->get_public_available_networks: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **float**| Optional latitude, if provided in addition to &#x60;lon&#x60; the network returned will only include network serving that location.  | [optional] 
 **lon** | **float**| Optional longitude, if provided in addition to &#x60;lat&#x60; the network returned will only include network serving that location.  | [optional] 
 **include_all_networks** | **bool**| Include additional supported networks that are filtered out by default. For example, these may include school busses and previews of upcoming agency network redesigns. | [optional] [default to false]
 **accept_language** | **str**| Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  | [optional] 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_public_latest_update_for_network**
> InlineResponse2008 get_public_latest_update_for_network(network_id=network_id, lat=lat, lon=lon)

Latest data update for network

Return the time of the most recent data update for a given network ID or network location

### Example
```python
from __future__ import print_function
import time
import transit_app_api
from transit_app_api.rest import ApiException
from pprint import pprint

# Configure API key authorization: apiKey
configuration = transit_app_api.Configuration()
configuration.api_key['apiKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# create an instance of the API class
api_instance = transit_app_api.PublicTransportApi(transit_app_api.ApiClient(configuration))
network_id = 'network_id_example' # str | Network ID or Network Location provided from [`/public/available_networks`](#operation/get-public-available_networks) (optional)
lat = 1.2 # float | Any lat of a location that serves that network. If provided, the performance of this call will be improved.  (optional)
lon = 1.2 # float | Any lon of a location that serves that network. If provided, the performance of this call will be improved.  (optional)

try:
    # Latest data update for network
    api_response = api_instance.get_public_latest_update_for_network(network_id=network_id, lat=lat, lon=lon)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicTransportApi->get_public_latest_update_for_network: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **network_id** | **str**| Network ID or Network Location provided from [&#x60;/public/available_networks&#x60;](#operation/get-public-available_networks) | [optional] 
 **lat** | **float**| Any lat of a location that serves that network. If provided, the performance of this call will be improved.  | [optional] 
 **lon** | **float**| Any lon of a location that serves that network. If provided, the performance of this call will be improved.  | [optional] 

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_public_route_details**
> InlineResponse2006 get_public_route_details(global_route_id, accept_language=accept_language)

Get detail for a route

Get detailed information like shape and itineraries for a route

### Example
```python
from __future__ import print_function
import time
import transit_app_api
from transit_app_api.rest import ApiException
from pprint import pprint

# Configure API key authorization: apiKey
configuration = transit_app_api.Configuration()
configuration.api_key['apiKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# create an instance of the API class
api_instance = transit_app_api.PublicTransportApi(transit_app_api.ApiClient(configuration))
global_route_id = 'global_route_id_example' # str | Global route id provided by other endpoint on which more detail is requested. 
accept_language = 'accept_language_example' # str | Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  (optional)

try:
    # Get detail for a route
    api_response = api_instance.get_public_route_details(global_route_id, accept_language=accept_language)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicTransportApi->get_public_route_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **global_route_id** | **str**| Global route id provided by other endpoint on which more detail is requested.  | 
 **accept_language** | **str**| Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  | [optional] 

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_public_routes_for_network**
> InlineResponse2007 get_public_routes_for_network(network_id=network_id, lat=lat, lon=lon, accept_language=accept_language)

Routes for a given network

Return all the routes for a given network.

### Example
```python
from __future__ import print_function
import time
import transit_app_api
from transit_app_api.rest import ApiException
from pprint import pprint

# Configure API key authorization: apiKey
configuration = transit_app_api.Configuration()
configuration.api_key['apiKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# create an instance of the API class
api_instance = transit_app_api.PublicTransportApi(transit_app_api.ApiClient(configuration))
network_id = 'network_id_example' # str | Network ID or Network Location provided from [`/public/available_networks`](#operation/get-public-available_networks) (optional)
lat = 1.2 # float | Any lat of a location that serves that network. If provided, the performance of this call will be improved.  (optional)
lon = 1.2 # float | Any lon of a location that serves that network. If provided, the performance of this call will be improved.  (optional)
accept_language = 'accept_language_example' # str | Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  (optional)

try:
    # Routes for a given network
    api_response = api_instance.get_public_routes_for_network(network_id=network_id, lat=lat, lon=lon, accept_language=accept_language)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicTransportApi->get_public_routes_for_network: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **network_id** | **str**| Network ID or Network Location provided from [&#x60;/public/available_networks&#x60;](#operation/get-public-available_networks) | [optional] 
 **lat** | **float**| Any lat of a location that serves that network. If provided, the performance of this call will be improved.  | [optional] 
 **lon** | **float**| Any lon of a location that serves that network. If provided, the performance of this call will be improved.  | [optional] 
 **accept_language** | **str**| Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  | [optional] 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_public_search_stops**
> InlineResponse2009 get_public_search_stops(lat=lat, lon=lon, query=query, pickup_dropoff_filter=pickup_dropoff_filter, max_num_results=max_num_results, accept_language=accept_language)

Find transit stops by search term

Given coordinates of an approximate area to search, find transit stops whose names or stop codes match the given search term, from feeds which serve the search area.

### Example
```python
from __future__ import print_function
import time
import transit_app_api
from transit_app_api.rest import ApiException
from pprint import pprint

# Configure API key authorization: apiKey
configuration = transit_app_api.Configuration()
configuration.api_key['apiKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# create an instance of the API class
api_instance = transit_app_api.PublicTransportApi(transit_app_api.ApiClient(configuration))
lat = 1.2 # float | Latitude of the approximate area of the search. (optional)
lon = 1.2 # float | Longitude of the approximate area of the search. (optional)
query = 'query_example' # str | Search term. Will be matched against the `stop_name` and `stop_code` of potential stops from the [GTFS](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#stopstxt). (optional)
pickup_dropoff_filter = 'pickup_dropoff_filter_example' # str | For routable stops, futher filter based on whether a rider can embark or disembark at this stop.   * `PickupAllowedOnly`: Riders can board at this stop on at least one trip.  * `DropoffAllowedOnly`: Riders can exit at this stop on at least one trip.  * `Everything`: All stops.    For further reference, see the [GTFS pickup_type and drop_off_type fields](https://developers.google.com/transit/gtfs/reference#stop_timestxt).'  (optional)
max_num_results = 56 # int | Maximum number of results to return. If there are few matches, less results than requested will be returned. (optional)
accept_language = 'accept_language_example' # str | Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  (optional)

try:
    # Find transit stops by search term
    api_response = api_instance.get_public_search_stops(lat=lat, lon=lon, query=query, pickup_dropoff_filter=pickup_dropoff_filter, max_num_results=max_num_results, accept_language=accept_language)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicTransportApi->get_public_search_stops: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **float**| Latitude of the approximate area of the search. | [optional] 
 **lon** | **float**| Longitude of the approximate area of the search. | [optional] 
 **query** | **str**| Search term. Will be matched against the &#x60;stop_name&#x60; and &#x60;stop_code&#x60; of potential stops from the [GTFS](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#stopstxt). | [optional] 
 **pickup_dropoff_filter** | **str**| For routable stops, futher filter based on whether a rider can embark or disembark at this stop.   * &#x60;PickupAllowedOnly&#x60;: Riders can board at this stop on at least one trip.  * &#x60;DropoffAllowedOnly&#x60;: Riders can exit at this stop on at least one trip.  * &#x60;Everything&#x60;: All stops.    For further reference, see the [GTFS pickup_type and drop_off_type fields](https://developers.google.com/transit/gtfs/reference#stop_timestxt).&#x27;  | [optional] 
 **max_num_results** | **int**| Maximum number of results to return. If there are few matches, less results than requested will be returned. | [optional] 
 **accept_language** | **str**| Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  | [optional] 

### Return type

[**InlineResponse2009**](InlineResponse2009.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_public_stops_for_network**
> InlineResponse200 get_public_stops_for_network(network_id=network_id, lat=lat, lon=lon, accept_language=accept_language)

Stops for a given network

Return all the stops for a given network.

### Example
```python
from __future__ import print_function
import time
import transit_app_api
from transit_app_api.rest import ApiException
from pprint import pprint

# Configure API key authorization: apiKey
configuration = transit_app_api.Configuration()
configuration.api_key['apiKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# create an instance of the API class
api_instance = transit_app_api.PublicTransportApi(transit_app_api.ApiClient(configuration))
network_id = 'network_id_example' # str | Network ID or Network Location provided from [`/public/available_networks`](#operation/get-public-available_networks) (optional)
lat = 1.2 # float | Any lat of a location that serves that network. If provided, the performance of this call will be improved.  (optional)
lon = 1.2 # float | Any lon of a location that serves that network. If provided, the performance of this call will be improved.  (optional)
accept_language = 'accept_language_example' # str | Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  (optional)

try:
    # Stops for a given network
    api_response = api_instance.get_public_stops_for_network(network_id=network_id, lat=lat, lon=lon, accept_language=accept_language)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicTransportApi->get_public_stops_for_network: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **network_id** | **str**| Network ID or Network Location provided from [&#x60;/public/available_networks&#x60;](#operation/get-public-available_networks) | [optional] 
 **lat** | **float**| Any lat of a location that serves that network. If provided, the performance of this call will be improved.  | [optional] 
 **lon** | **float**| Any lon of a location that serves that network. If provided, the performance of this call will be improved.  | [optional] 
 **accept_language** | **str**| Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  | [optional] 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_v3_public_trip_details**
> InlineResponse20010 get_v3_public_trip_details(trip_search_key, accept_language=accept_language)

Get details for a trip.

Provides scheduled times and stop information for an entire trip

### Example
```python
from __future__ import print_function
import time
import transit_app_api
from transit_app_api.rest import ApiException
from pprint import pprint

# Configure API key authorization: apiKey
configuration = transit_app_api.Configuration()
configuration.api_key['apiKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# create an instance of the API class
api_instance = transit_app_api.PublicTransportApi(transit_app_api.ApiClient(configuration))
trip_search_key = 'trip_search_key_example' # str | A trip identifier obtained from other endpoints like `/v3/public/stop_departures`. This value will frequently change as feeds are updated and should be refetched regularly before use in further requests.
accept_language = 'accept_language_example' # str | Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  (optional)

try:
    # Get details for a trip.
    api_response = api_instance.get_v3_public_trip_details(trip_search_key, accept_language=accept_language)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicTransportApi->get_v3_public_trip_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **trip_search_key** | **str**| A trip identifier obtained from other endpoints like &#x60;/v3/public/stop_departures&#x60;. This value will frequently change as feeds are updated and should be refetched regularly before use in further requests. | 
 **accept_language** | **str**| Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  | [optional] 

### Return type

[**InlineResponse20010**](InlineResponse20010.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **nearby_stops**
> InlineResponse200 nearby_stops(lat, lon, max_distance=max_distance, stop_filter=stop_filter, pickup_dropoff_filter=pickup_dropoff_filter, accept_language=accept_language)

Get transit stops which are near a location

Returns stops around a location

### Example
```python
from __future__ import print_function
import time
import transit_app_api
from transit_app_api.rest import ApiException
from pprint import pprint

# Configure API key authorization: apiKey
configuration = transit_app_api.Configuration()
configuration.api_key['apiKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# create an instance of the API class
api_instance = transit_app_api.PublicTransportApi(transit_app_api.ApiClient(configuration))
lat = 1.2 # float | Latitude
lon = 1.2 # float | Longitude
max_distance = 150 # int | Maximum radius of search from the request location (in meters) (optional) (default to 150)
stop_filter = 'Routable' # str | Determines which location types from the [GTFS](https://developers.google.com/transit/gtfs/reference#stopstxt) are included in the response.  * Routable (GTFS location_type 0): Stops which are served by routes * EntrancesAndStopsOutsideStations: Entrances to transit stations and freestanding outdoor stops * Entrances (GTFS location_type 2): Entrances to stations * Any: All stops  (optional) (default to Routable)
pickup_dropoff_filter = 'pickup_dropoff_filter_example' # str | For routable stops, futher filter based on whether a rider can embark or disembark at this stop.   * `PickupAllowedOnly`: Riders can board at this stop on at least one trip.  * `DropoffAllowedOnly`: Riders can exit at this stop on at least one trip.  * `Everything`: All stops.    For further reference, see the [GTFS pickup_type and drop_off_type fields](https://developers.google.com/transit/gtfs/reference#stop_timestxt).'  (optional)
accept_language = 'accept_language_example' # str | Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  (optional)

try:
    # Get transit stops which are near a location
    api_response = api_instance.nearby_stops(lat, lon, max_distance=max_distance, stop_filter=stop_filter, pickup_dropoff_filter=pickup_dropoff_filter, accept_language=accept_language)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicTransportApi->nearby_stops: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **float**| Latitude | 
 **lon** | **float**| Longitude | 
 **max_distance** | **int**| Maximum radius of search from the request location (in meters) | [optional] [default to 150]
 **stop_filter** | **str**| Determines which location types from the [GTFS](https://developers.google.com/transit/gtfs/reference#stopstxt) are included in the response.  * Routable (GTFS location_type 0): Stops which are served by routes * EntrancesAndStopsOutsideStations: Entrances to transit stations and freestanding outdoor stops * Entrances (GTFS location_type 2): Entrances to stations * Any: All stops  | [optional] [default to Routable]
 **pickup_dropoff_filter** | **str**| For routable stops, futher filter based on whether a rider can embark or disembark at this stop.   * &#x60;PickupAllowedOnly&#x60;: Riders can board at this stop on at least one trip.  * &#x60;DropoffAllowedOnly&#x60;: Riders can exit at this stop on at least one trip.  * &#x60;Everything&#x60;: All stops.    For further reference, see the [GTFS pickup_type and drop_off_type fields](https://developers.google.com/transit/gtfs/reference#stop_timestxt).&#x27;  | [optional] 
 **accept_language** | **str**| Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  | [optional] 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **otp**
> InlineResponse2002 otp(from_place, to_place, arrive_by=arrive_by, _date=_date, time=time, mode=mode, num_itineraries=num_itineraries, locale=locale, walk_reluctance=walk_reluctance, wheelchair=wheelchair, ignore_real_time_updates=ignore_real_time_updates, allowed_networks=allowed_networks, accept_language=accept_language)

Plan a trip from origin to destination

Except as noted, the API is compatible with the OpenTripPlanner API. For additional information, you may refer to <http://dev.opentripplanner.org/apidoc/2.0.0/resource_PlannerResource.html>. 

### Example
```python
from __future__ import print_function
import time
import transit_app_api
from transit_app_api.rest import ApiException
from pprint import pprint

# Configure API key authorization: apiKey
configuration = transit_app_api.Configuration()
configuration.api_key['apiKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# create an instance of the API class
api_instance = transit_app_api.PublicTransportApi(transit_app_api.ApiClient(configuration))
from_place = 'from_place_example' # str | Originating location for the trip.  * Simple lat/lon pair (e.g. `40.714476,-74.005966`) * Lat/lon pair with label (e.g. `289 Broadway::40.714476,-74.005966`) 
to_place = 'to_place_example' # str | Destination location for the trip. * Simple lat/lon pair (e.g. `40.714476,-74.005966`) * Lat/lon pair with label (e.g. `289 Broadway::40.714476,-74.005966`) 
arrive_by = false # bool | Selects 'leave after' or 'arrive by' planning.    | Case    |  Description                           |   |---------|----------------------------------------|   | `false` | Trip departs after `date` and `time`.  |   | `true`  | Trip arrives before `date` and `time`. |  (optional) (default to false)
_date = 'Current time when request was issued.' # str | Date of departure or arrival. Must be in UTC. (optional) (default to Current time when request was issued.)
time = 'Current time when request was issued.' # str | Time of departure or arrival. Must be in UTC. (optional) (default to Current time when request was issued.)
mode = 'TRANSIT,WALK' # str | The following combinations of mode are currently supported.  <div class='wide-table'>   | 1st Mode                | 2nd Mode                             | Description                      |  |-------------------------|--------------------------------------|----------------------------------|  | `TRANSIT`               | `WALK`                               | Use transit only. (default)      |  | `BICYCLE`               | *(none)*                             | Use personal bike only.          |  | `WALK`                  | *(none)*                             | Walk only.                       |  | `MICROTRANSIT`          | *(none)*                             | Use on-demand public transit services only. |  | `TRANSIT`               | `BICYCLE_FirstLeg` or `BICYCLE`      | Use personal bike for first leg, transit for rest of trip. |  | `TRANSIT`               | `BICYCLE_LastLeg`                    | Use personal bike for last leg, transit for rest of trip. |  | `TRANSIT`               | `BICYCLE_FirstAndLastLegs`           | Use personal bike for the first and last legs of trip, transit for the remaining legs. |  | `TRANSIT`               | `BICYCLE_RENT`                       | Use bikeshare for the first and last legs of trip, transit for the remaining legs. Docked and dockless bikes, as well as scooters are included. |  | `TRANSIT`               | `BICYCLE_RENT_Bikeshare`             | Use docked bikes for first and last legs of trip. |  | `TRANSIT`               | `BICYCLE_RENT_DocklessBikes`         | Use dockless bikes for the first and last legs of trip. |  | `TRANSIT`               | `BICYLCE_RENT_ElectricScooter`       | Use scooters for the first and last legs of trip. |  | `TRANSIT`               | `MICROTRANSIT`                       | Use on-demand public transit services for the first and last legs of the trip, and scheduled services for the remaining legs. |   </div>  <style>         code { word-break: keep-all !important; whitespace: pre !important; }         .wide-table table td, .wide-table table th { display: inline-block; width: 40%; border: 0}         .wide-table table td:nth-child(3), .wide-table table th:nth-child(3) { width: 100% }         .wide-table table th:nth-child(3) { visibility: hidden}   </style>   (optional) (default to TRANSIT,WALK)
num_itineraries = 3 # int | The maximum number of possible itineraries to return. (optional) (default to 3)
locale = 'en' # str | Language to be used for names in response (optional) (default to en)
walk_reluctance = 2 # float | Walking is minimized if walkReluctance &geq; 20.0. (optional) (default to 2)
wheelchair = false # bool | Whether the trip must be wheelchair accessible. (optional) (default to false)
ignore_real_time_updates = false # bool | If false, times within trip plans reflect real-time information if available. (optional) (default to false)
allowed_networks = 'allowed_networks_example' # str | If set, only the specified networks will be used to plan trips. A list of available networks can be obtained from [`/public/available_networks`](#operation/get-public-available_networks).   This parameter will accept a comma-separated list containing network IDs, network locations or a combination of both. Provides similar functionality to the OTP parameter `whitelistedAgencies`.  (optional)
accept_language = 'accept_language_example' # str | Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  (optional)

try:
    # Plan a trip from origin to destination
    api_response = api_instance.otp(from_place, to_place, arrive_by=arrive_by, _date=_date, time=time, mode=mode, num_itineraries=num_itineraries, locale=locale, walk_reluctance=walk_reluctance, wheelchair=wheelchair, ignore_real_time_updates=ignore_real_time_updates, allowed_networks=allowed_networks, accept_language=accept_language)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicTransportApi->otp: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from_place** | **str**| Originating location for the trip.  * Simple lat/lon pair (e.g. &#x60;40.714476,-74.005966&#x60;) * Lat/lon pair with label (e.g. &#x60;289 Broadway::40.714476,-74.005966&#x60;)  | 
 **to_place** | **str**| Destination location for the trip. * Simple lat/lon pair (e.g. &#x60;40.714476,-74.005966&#x60;) * Lat/lon pair with label (e.g. &#x60;289 Broadway::40.714476,-74.005966&#x60;)  | 
 **arrive_by** | **bool**| Selects &#x27;leave after&#x27; or &#x27;arrive by&#x27; planning.    | Case    |  Description                           |   |---------|----------------------------------------|   | &#x60;false&#x60; | Trip departs after &#x60;date&#x60; and &#x60;time&#x60;.  |   | &#x60;true&#x60;  | Trip arrives before &#x60;date&#x60; and &#x60;time&#x60;. |  | [optional] [default to false]
 **_date** | **str**| Date of departure or arrival. Must be in UTC. | [optional] [default to Current time when request was issued.]
 **time** | **str**| Time of departure or arrival. Must be in UTC. | [optional] [default to Current time when request was issued.]
 **mode** | **str**| The following combinations of mode are currently supported.  &lt;div class&#x3D;&#x27;wide-table&#x27;&gt;   | 1st Mode                | 2nd Mode                             | Description                      |  |-------------------------|--------------------------------------|----------------------------------|  | &#x60;TRANSIT&#x60;               | &#x60;WALK&#x60;                               | Use transit only. (default)      |  | &#x60;BICYCLE&#x60;               | *(none)*                             | Use personal bike only.          |  | &#x60;WALK&#x60;                  | *(none)*                             | Walk only.                       |  | &#x60;MICROTRANSIT&#x60;          | *(none)*                             | Use on-demand public transit services only. |  | &#x60;TRANSIT&#x60;               | &#x60;BICYCLE_FirstLeg&#x60; or &#x60;BICYCLE&#x60;      | Use personal bike for first leg, transit for rest of trip. |  | &#x60;TRANSIT&#x60;               | &#x60;BICYCLE_LastLeg&#x60;                    | Use personal bike for last leg, transit for rest of trip. |  | &#x60;TRANSIT&#x60;               | &#x60;BICYCLE_FirstAndLastLegs&#x60;           | Use personal bike for the first and last legs of trip, transit for the remaining legs. |  | &#x60;TRANSIT&#x60;               | &#x60;BICYCLE_RENT&#x60;                       | Use bikeshare for the first and last legs of trip, transit for the remaining legs. Docked and dockless bikes, as well as scooters are included. |  | &#x60;TRANSIT&#x60;               | &#x60;BICYCLE_RENT_Bikeshare&#x60;             | Use docked bikes for first and last legs of trip. |  | &#x60;TRANSIT&#x60;               | &#x60;BICYCLE_RENT_DocklessBikes&#x60;         | Use dockless bikes for the first and last legs of trip. |  | &#x60;TRANSIT&#x60;               | &#x60;BICYLCE_RENT_ElectricScooter&#x60;       | Use scooters for the first and last legs of trip. |  | &#x60;TRANSIT&#x60;               | &#x60;MICROTRANSIT&#x60;                       | Use on-demand public transit services for the first and last legs of the trip, and scheduled services for the remaining legs. |   &lt;/div&gt;  &lt;style&gt;         code { word-break: keep-all !important; whitespace: pre !important; }         .wide-table table td, .wide-table table th { display: inline-block; width: 40%; border: 0}         .wide-table table td:nth-child(3), .wide-table table th:nth-child(3) { width: 100% }         .wide-table table th:nth-child(3) { visibility: hidden}   &lt;/style&gt;   | [optional] [default to TRANSIT,WALK]
 **num_itineraries** | **int**| The maximum number of possible itineraries to return. | [optional] [default to 3]
 **locale** | **str**| Language to be used for names in response | [optional] [default to en]
 **walk_reluctance** | **float**| Walking is minimized if walkReluctance &amp;geq; 20.0. | [optional] [default to 2]
 **wheelchair** | **bool**| Whether the trip must be wheelchair accessible. | [optional] [default to false]
 **ignore_real_time_updates** | **bool**| If false, times within trip plans reflect real-time information if available. | [optional] [default to false]
 **allowed_networks** | **str**| If set, only the specified networks will be used to plan trips. A list of available networks can be obtained from [&#x60;/public/available_networks&#x60;](#operation/get-public-available_networks).   This parameter will accept a comma-separated list containing network IDs, network locations or a combination of both. Provides similar functionality to the OTP parameter &#x60;whitelistedAgencies&#x60;.  | [optional] 
 **accept_language** | **str**| Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stop_departures**
> InlineResponse2001 stop_departures(global_stop_id, time=time, remove_cancelled=remove_cancelled, accept_language=accept_language)

Get upcoming departures for all routes serving a stop

Get upcoming departures for all routes serving a stop, with real time information. 

### Example
```python
from __future__ import print_function
import time
import transit_app_api
from transit_app_api.rest import ApiException
from pprint import pprint

# Configure API key authorization: apiKey
configuration = transit_app_api.Configuration()
configuration.api_key['apiKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# create an instance of the API class
api_instance = transit_app_api.PublicTransportApi(transit_app_api.ApiClient(configuration))
global_stop_id = 'global_stop_id_example' # str | A global stop ID, representing a routable stop. Usually this value will be reused from a previous call (ex : nearby_stops)
time = 1.2 # float | UNIX timestamp representing the time for which departures should be determined (optional)
remove_cancelled = false # bool | Remove cancelled schedule items from the results (optional) (default to false)
accept_language = 'accept_language_example' # str | Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  (optional)

try:
    # Get upcoming departures for all routes serving a stop
    api_response = api_instance.stop_departures(global_stop_id, time=time, remove_cancelled=remove_cancelled, accept_language=accept_language)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicTransportApi->stop_departures: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **global_stop_id** | **str**| A global stop ID, representing a routable stop. Usually this value will be reused from a previous call (ex : nearby_stops) | 
 **time** | **float**| UNIX timestamp representing the time for which departures should be determined | [optional] 
 **remove_cancelled** | **bool**| Remove cancelled schedule items from the results | [optional] [default to false]
 **accept_language** | **str**| Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

