# Itinerary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**direction_id** | [**DirectionId**](DirectionId.md) |  | 
**headsign** | **str** | The headsign from the GTFS. If a stop_headsign is provided in the GTFS, it will be provided here. Refer to the [trip_headsign and stop_headsign](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#tripstxt) for more information.  | 
**schedule_items** | [**list[ScheduleItem]**](ScheduleItem.md) |  | 
**branch_code** | **str** | Branch code for that itinerary. Branch are short string associated with an itinerary that represent version of the route. Ex : The 55 bus has &#x27;A&#x27; and &#x27;B&#x27; variant. If no branch are present, an empty string is provided.  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

