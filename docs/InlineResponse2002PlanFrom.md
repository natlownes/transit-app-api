# InlineResponse2002PlanFrom

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **float** |  | 
**lon** | **float** |  | 
**name** | **str** | The name of the transit stop or other point | 
**vertex_type** | [**VertexType**](VertexType.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

