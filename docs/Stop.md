# Stop

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distance** | **float** | Distance from the query point in meters. Not provided in &#x60;route_details&#x60; or when there&#x27;s no location in the query.  | [optional] 
**global_stop_id** | [**GlobalStopId**](GlobalStopId.md) |  | 
**location_type** | **float** | &#x60;location_types&#x60; from the [GTFS](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#stopstxt). The following values may be returned:  * &#x60;0&#x60;: Routable stops (whether freestanding or located within a station)  * &#x60;2&#x60;: Entrances to stations     | 
**parent_station_global_stop_id** | **str** | This field can be used to identify routable stops or entrances that are part of the same station. If applicable, this field contains a &#x60;GlobalStopId&#x60; referring to the containing station | [optional] 
**route_type** | **int** | For routable stops, route type from the [GTFS](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#routestxt) | [optional] 
**stop_lat** | **float** |  | 
**stop_lon** | **float** |  | 
**stop_name** | **str** | Stop name from the [GTFS](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#stopstxt)&#x27; | 
**stop_code** | **str** | &#x60;stop_code&#x60; of the stop, from the [GTFS](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#stopstxt) | [optional] 
**rt_stop_id** | **str** | A identifier for this stop. In the majority of cases, it will be the same &#x60;stop_id&#x60; found from the original GTFS. Can be an empty string if no stop id is provided.  | [optional] 
**wheelchair_boarding** | **int** | &#x60;wheelchair_boarding&#x60; of the stop, from the [GTFS](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#stopstxt) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

