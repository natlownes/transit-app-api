# ScheduleItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**departure_time** | **float** | Departure time of the schedule item in UNIX time.  If &#x60;is_real_time&#x60; is false, it will be equal to &#x60;scheduled_departure_time&#x60;.  | 
**is_cancelled** | **bool** | If this departure has been cancelled. Cancelled departures should either be crossed of or not shown at all in a UI.  | 
**is_real_time** | **bool** | If the departure_time is based on real time data.  | 
**rt_trip_id** | **str** | A identifier for that trip. In the majority of cases, it will be the same &#x60;trip_id&#x60; found from the original GTFS. Note that two departures can have the same &#x60;rt_trip_id&#x60; if one of them has been duplicated by real time data. Can be empty string if no trip id is provided.  | [optional] 
**scheduled_departure_time** | **float** | Departure time based on schedule information in UNIX time. | 
**wheelchair_accessible** | **int** | &#x60;wheelchair_accessible&#x60; of the corresponding trip, from the [GTFS](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#tripstxt) | [optional] 
**trip_search_key** | [**TripSearchKey**](TripSearchKey.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

