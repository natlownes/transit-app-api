# InlineResponse2002PlanIntermediateStops

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **float** |  | 
**lon** | **float** |  | 
**name** | **str** | &#x60;stop_name&#x60; from the [GTFS](https://developers.google.com/transit/gtfs/reference#stopstxt) | 
**stop_code** | **str** | &#x60;stop_code&#x60; from the [GTFS](https://developers.google.com/transit/gtfs/reference#stopstxt) | 
**stop_id** | **str** | Correlates with real-time data when available | 
**global_stop_id** | [**GlobalStopId**](GlobalStopId.md) |  | [optional] 
**stop_index** | **float** | Represents the order of the stop within the GTFS trip. For example, stopIndex &#x3D; 5 means that rider should remain on the bus through the 6th stop of the trip. | 
**vertex_type** | [**VertexType**](VertexType.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

