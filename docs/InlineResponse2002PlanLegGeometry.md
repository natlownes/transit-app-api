# InlineResponse2002PlanLegGeometry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**length** | **float** | Number of vertices encoded in points | 
**points** | **str** | A Polyline. The format is specified in &lt;https://developers.google.com/maps/documentation/utilities/polylinealgorithm&gt; | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

