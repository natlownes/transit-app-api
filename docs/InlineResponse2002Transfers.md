# InlineResponse2002Transfers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**buffer_time** | **float** |  | 
**default_minimum_time** | **float** |  | 
**minimum_times** | [**list[InlineResponse2002TransfersMinimumTimes]**](InlineResponse2002TransfersMinimumTimes.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

