# InlineResponse2002

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plan** | [**InlineResponse2002Plan**](InlineResponse2002Plan.md) |  | 
**transfers** | [**InlineResponse2002Transfers**](InlineResponse2002Transfers.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

