# InlineResponse2005Networks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**network_geometry** | **object** | GeoJSON (of type Polygon) representing the service provided by the network. | [optional] 
**network_id** | **int** | Identifier of the network. | [optional] 
**network_name** | **str** | Name of the network. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

