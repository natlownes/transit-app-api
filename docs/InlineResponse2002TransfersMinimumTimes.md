# InlineResponse2002TransfersMinimumTimes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from_feed_id** | **float** |  | 
**from_stop_id** | **float** |  | 
**min_time** | **float** |  | 
**to_feed_id** | **float** |  | 
**to_stop_id** | **float** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

