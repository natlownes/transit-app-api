# InlineResponse20010ScheduleItems

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**departure_time** | **float** | Time of departure at this stop, in UNIX time in seconds. | 
**stop** | [**Stop**](Stop.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

