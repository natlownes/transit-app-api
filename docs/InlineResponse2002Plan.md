# InlineResponse2002Plan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_date** | **float** | Date on which the trip takes place, represented in UNIX time in milliseconds. | 
**_from** | [**InlineResponse2002PlanFrom**](InlineResponse2002PlanFrom.md) |  | 
**itineraries** | [**list[InlineResponse2002PlanItineraries]**](InlineResponse2002PlanItineraries.md) |  | 
**to** | [**InlineResponse2002PlanFrom**](InlineResponse2002PlanFrom.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

