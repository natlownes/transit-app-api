# FloatingPlacemark

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** | Human readable description of the vehicle | [optional] 
**id** | **str** | External identifier from micromobility data provider | [optional] 
**network_name** | **str** | Name of the operator of the network (ie Lyft, CoGo) | [optional] 
**network_id** | **float** | Unique identifier for the network (ie Lyft in Los Angeles, CoGo in Columbus) | [optional] 
**color** | **str** | Main brand color of the network in hex format (no leading #) | [optional] 
**text_color** | **str** | Contrasting color for use over the brand color in hex format (no leading #) | [optional] 
**latitude** | **float** | Latitude of the vehicle | [optional] 
**longitude** | **float** | Longitude of the vehicle | [optional] 
**type** | [**VehicleType**](VehicleType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

