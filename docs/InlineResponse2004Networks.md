# InlineResponse2004Networks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**network_geometry** | **object** | GeoJSON (of type Polygon or MultiPolygon) representing the service provided by the network.  | 
**network_id** | [**NetworkId**](NetworkId.md) |  | 
**network_name** | **str** | Name of the network.  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

