# transit_app_api.SharedMobilityApi

All URIs are relative to *https://external.transitapp.com/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_placemarks**](SharedMobilityApi.md#get_placemarks) | **GET** /map_layers/placemarks | Get a list of placemarks
[**get_sharing_system_available_networks**](SharedMobilityApi.md#get_sharing_system_available_networks) | **GET** /map_layers/available_networks | Get a list of available sharing system networks
[**otp**](SharedMobilityApi.md#otp) | **GET** /otp/plan | Plan a trip from origin to destination

# **get_placemarks**
> InlineResponse2003 get_placemarks(lat, lon, distance=distance)

Get a list of placemarks

Returns placemarks for a location. Placemarks include bikeshare system docks, scooters and other shared systems. 

### Example
```python
from __future__ import print_function
import time
import transit_app_api
from transit_app_api.rest import ApiException
from pprint import pprint

# Configure API key authorization: apiKey
configuration = transit_app_api.Configuration()
configuration.api_key['apiKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# create an instance of the API class
api_instance = transit_app_api.SharedMobilityApi(transit_app_api.ApiClient(configuration))
lat = 1.2 # float | Latitude
lon = 1.2 # float | Longitude
distance = 100 # int | Distance in meters (as the bird flies).  (optional) (default to 100)

try:
    # Get a list of placemarks
    api_response = api_instance.get_placemarks(lat, lon, distance=distance)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SharedMobilityApi->get_placemarks: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **float**| Latitude | 
 **lon** | **float**| Longitude | 
 **distance** | **int**| Distance in meters (as the bird flies).  | [optional] [default to 100]

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_sharing_system_available_networks**
> InlineResponse2005 get_sharing_system_available_networks()

Get a list of available sharing system networks

List of sharing system networks available in the entire Transit system.

### Example
```python
from __future__ import print_function
import time
import transit_app_api
from transit_app_api.rest import ApiException
from pprint import pprint

# Configure API key authorization: apiKey
configuration = transit_app_api.Configuration()
configuration.api_key['apiKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# create an instance of the API class
api_instance = transit_app_api.SharedMobilityApi(transit_app_api.ApiClient(configuration))

try:
    # Get a list of available sharing system networks
    api_response = api_instance.get_sharing_system_available_networks()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SharedMobilityApi->get_sharing_system_available_networks: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **otp**
> InlineResponse2002 otp(from_place, to_place, arrive_by=arrive_by, _date=_date, time=time, mode=mode, num_itineraries=num_itineraries, locale=locale, walk_reluctance=walk_reluctance, wheelchair=wheelchair, ignore_real_time_updates=ignore_real_time_updates, allowed_networks=allowed_networks, accept_language=accept_language)

Plan a trip from origin to destination

Except as noted, the API is compatible with the OpenTripPlanner API. For additional information, you may refer to <http://dev.opentripplanner.org/apidoc/2.0.0/resource_PlannerResource.html>. 

### Example
```python
from __future__ import print_function
import time
import transit_app_api
from transit_app_api.rest import ApiException
from pprint import pprint

# Configure API key authorization: apiKey
configuration = transit_app_api.Configuration()
configuration.api_key['apiKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKey'] = 'Bearer'

# create an instance of the API class
api_instance = transit_app_api.SharedMobilityApi(transit_app_api.ApiClient(configuration))
from_place = 'from_place_example' # str | Originating location for the trip.  * Simple lat/lon pair (e.g. `40.714476,-74.005966`) * Lat/lon pair with label (e.g. `289 Broadway::40.714476,-74.005966`) 
to_place = 'to_place_example' # str | Destination location for the trip. * Simple lat/lon pair (e.g. `40.714476,-74.005966`) * Lat/lon pair with label (e.g. `289 Broadway::40.714476,-74.005966`) 
arrive_by = false # bool | Selects 'leave after' or 'arrive by' planning.    | Case    |  Description                           |   |---------|----------------------------------------|   | `false` | Trip departs after `date` and `time`.  |   | `true`  | Trip arrives before `date` and `time`. |  (optional) (default to false)
_date = 'Current time when request was issued.' # str | Date of departure or arrival. Must be in UTC. (optional) (default to Current time when request was issued.)
time = 'Current time when request was issued.' # str | Time of departure or arrival. Must be in UTC. (optional) (default to Current time when request was issued.)
mode = 'TRANSIT,WALK' # str | The following combinations of mode are currently supported.  <div class='wide-table'>   | 1st Mode                | 2nd Mode                             | Description                      |  |-------------------------|--------------------------------------|----------------------------------|  | `TRANSIT`               | `WALK`                               | Use transit only. (default)      |  | `BICYCLE`               | *(none)*                             | Use personal bike only.          |  | `WALK`                  | *(none)*                             | Walk only.                       |  | `MICROTRANSIT`          | *(none)*                             | Use on-demand public transit services only. |  | `TRANSIT`               | `BICYCLE_FirstLeg` or `BICYCLE`      | Use personal bike for first leg, transit for rest of trip. |  | `TRANSIT`               | `BICYCLE_LastLeg`                    | Use personal bike for last leg, transit for rest of trip. |  | `TRANSIT`               | `BICYCLE_FirstAndLastLegs`           | Use personal bike for the first and last legs of trip, transit for the remaining legs. |  | `TRANSIT`               | `BICYCLE_RENT`                       | Use bikeshare for the first and last legs of trip, transit for the remaining legs. Docked and dockless bikes, as well as scooters are included. |  | `TRANSIT`               | `BICYCLE_RENT_Bikeshare`             | Use docked bikes for first and last legs of trip. |  | `TRANSIT`               | `BICYCLE_RENT_DocklessBikes`         | Use dockless bikes for the first and last legs of trip. |  | `TRANSIT`               | `BICYLCE_RENT_ElectricScooter`       | Use scooters for the first and last legs of trip. |  | `TRANSIT`               | `MICROTRANSIT`                       | Use on-demand public transit services for the first and last legs of the trip, and scheduled services for the remaining legs. |   </div>  <style>         code { word-break: keep-all !important; whitespace: pre !important; }         .wide-table table td, .wide-table table th { display: inline-block; width: 40%; border: 0}         .wide-table table td:nth-child(3), .wide-table table th:nth-child(3) { width: 100% }         .wide-table table th:nth-child(3) { visibility: hidden}   </style>   (optional) (default to TRANSIT,WALK)
num_itineraries = 3 # int | The maximum number of possible itineraries to return. (optional) (default to 3)
locale = 'en' # str | Language to be used for names in response (optional) (default to en)
walk_reluctance = 2 # float | Walking is minimized if walkReluctance &geq; 20.0. (optional) (default to 2)
wheelchair = false # bool | Whether the trip must be wheelchair accessible. (optional) (default to false)
ignore_real_time_updates = false # bool | If false, times within trip plans reflect real-time information if available. (optional) (default to false)
allowed_networks = 'allowed_networks_example' # str | If set, only the specified networks will be used to plan trips. A list of available networks can be obtained from [`/public/available_networks`](#operation/get-public-available_networks).   This parameter will accept a comma-separated list containing network IDs, network locations or a combination of both. Provides similar functionality to the OTP parameter `whitelistedAgencies`.  (optional)
accept_language = 'accept_language_example' # str | Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  (optional)

try:
    # Plan a trip from origin to destination
    api_response = api_instance.otp(from_place, to_place, arrive_by=arrive_by, _date=_date, time=time, mode=mode, num_itineraries=num_itineraries, locale=locale, walk_reluctance=walk_reluctance, wheelchair=wheelchair, ignore_real_time_updates=ignore_real_time_updates, allowed_networks=allowed_networks, accept_language=accept_language)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SharedMobilityApi->otp: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from_place** | **str**| Originating location for the trip.  * Simple lat/lon pair (e.g. &#x60;40.714476,-74.005966&#x60;) * Lat/lon pair with label (e.g. &#x60;289 Broadway::40.714476,-74.005966&#x60;)  | 
 **to_place** | **str**| Destination location for the trip. * Simple lat/lon pair (e.g. &#x60;40.714476,-74.005966&#x60;) * Lat/lon pair with label (e.g. &#x60;289 Broadway::40.714476,-74.005966&#x60;)  | 
 **arrive_by** | **bool**| Selects &#x27;leave after&#x27; or &#x27;arrive by&#x27; planning.    | Case    |  Description                           |   |---------|----------------------------------------|   | &#x60;false&#x60; | Trip departs after &#x60;date&#x60; and &#x60;time&#x60;.  |   | &#x60;true&#x60;  | Trip arrives before &#x60;date&#x60; and &#x60;time&#x60;. |  | [optional] [default to false]
 **_date** | **str**| Date of departure or arrival. Must be in UTC. | [optional] [default to Current time when request was issued.]
 **time** | **str**| Time of departure or arrival. Must be in UTC. | [optional] [default to Current time when request was issued.]
 **mode** | **str**| The following combinations of mode are currently supported.  &lt;div class&#x3D;&#x27;wide-table&#x27;&gt;   | 1st Mode                | 2nd Mode                             | Description                      |  |-------------------------|--------------------------------------|----------------------------------|  | &#x60;TRANSIT&#x60;               | &#x60;WALK&#x60;                               | Use transit only. (default)      |  | &#x60;BICYCLE&#x60;               | *(none)*                             | Use personal bike only.          |  | &#x60;WALK&#x60;                  | *(none)*                             | Walk only.                       |  | &#x60;MICROTRANSIT&#x60;          | *(none)*                             | Use on-demand public transit services only. |  | &#x60;TRANSIT&#x60;               | &#x60;BICYCLE_FirstLeg&#x60; or &#x60;BICYCLE&#x60;      | Use personal bike for first leg, transit for rest of trip. |  | &#x60;TRANSIT&#x60;               | &#x60;BICYCLE_LastLeg&#x60;                    | Use personal bike for last leg, transit for rest of trip. |  | &#x60;TRANSIT&#x60;               | &#x60;BICYCLE_FirstAndLastLegs&#x60;           | Use personal bike for the first and last legs of trip, transit for the remaining legs. |  | &#x60;TRANSIT&#x60;               | &#x60;BICYCLE_RENT&#x60;                       | Use bikeshare for the first and last legs of trip, transit for the remaining legs. Docked and dockless bikes, as well as scooters are included. |  | &#x60;TRANSIT&#x60;               | &#x60;BICYCLE_RENT_Bikeshare&#x60;             | Use docked bikes for first and last legs of trip. |  | &#x60;TRANSIT&#x60;               | &#x60;BICYCLE_RENT_DocklessBikes&#x60;         | Use dockless bikes for the first and last legs of trip. |  | &#x60;TRANSIT&#x60;               | &#x60;BICYLCE_RENT_ElectricScooter&#x60;       | Use scooters for the first and last legs of trip. |  | &#x60;TRANSIT&#x60;               | &#x60;MICROTRANSIT&#x60;                       | Use on-demand public transit services for the first and last legs of the trip, and scheduled services for the remaining legs. |   &lt;/div&gt;  &lt;style&gt;         code { word-break: keep-all !important; whitespace: pre !important; }         .wide-table table td, .wide-table table th { display: inline-block; width: 40%; border: 0}         .wide-table table td:nth-child(3), .wide-table table th:nth-child(3) { width: 100% }         .wide-table table th:nth-child(3) { visibility: hidden}   &lt;/style&gt;   | [optional] [default to TRANSIT,WALK]
 **num_itineraries** | **int**| The maximum number of possible itineraries to return. | [optional] [default to 3]
 **locale** | **str**| Language to be used for names in response | [optional] [default to en]
 **walk_reluctance** | **float**| Walking is minimized if walkReluctance &amp;geq; 20.0. | [optional] [default to 2]
 **wheelchair** | **bool**| Whether the trip must be wheelchair accessible. | [optional] [default to false]
 **ignore_real_time_updates** | **bool**| If false, times within trip plans reflect real-time information if available. | [optional] [default to false]
 **allowed_networks** | **str**| If set, only the specified networks will be used to plan trips. A list of available networks can be obtained from [&#x60;/public/available_networks&#x60;](#operation/get-public-available_networks).   This parameter will accept a comma-separated list containing network IDs, network locations or a combination of both. Provides similar functionality to the OTP parameter &#x60;whitelistedAgencies&#x60;.  | [optional] 
 **accept_language** | **str**| Names and other strings can translated into any of the supported languages of a feed. If not provided, the default language of the feed is selected.  | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

