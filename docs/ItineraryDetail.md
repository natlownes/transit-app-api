# ItineraryDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**direction_headsign** | **str** | Direction headsign that represent the overall direction of the trip. Ex : \&quot;Northbound\&quot;.  | [optional] 
**direction_id** | [**DirectionId**](DirectionId.md) |  | 
**headsign** | **str** | The headsign from the GTFS. Refer to the [trip_headsign](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#tripstxt) for more information. | 
**shape** | **str** | Shape provided in the [encoded polyline format](https://developers.google.com/maps/documentation/utilities/polylinealgorithm). If no shape is provided, null will be returned.  | [optional] 
**stops** | [**list[Stop]**](Stop.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

