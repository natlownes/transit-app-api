# StationPlacemark

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** | Description of the location of the docking station (ie High St / Warren) | [optional] 
**subtitle** | **str** | Human readable description of vehicles available | [optional] 
**id** | **str** | External identifier from micromobility data provider | [optional] 
**network_name** | **str** | Name of the operator of the network | [optional] 
**network_id** | **float** | Unique identifier for the network (ie Lyft in Los Angeles, CoGo in Columbus) | [optional] 
**color** | **str** | Main brand color of the network in hex format (no leading #) | [optional] 
**text_color** | **str** | Contrasting color for use over the brand color in hex format (no leading #) | [optional] 
**latitude** | **float** | Latitude of the station | [optional] 
**longitude** | **float** | Longitude of the station | [optional] 
**type** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

