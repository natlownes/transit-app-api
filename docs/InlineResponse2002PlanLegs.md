# InlineResponse2002PlanLegs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agency_id** | **str** | Identifies the transit agency or sharing system provider operating the service used for this leg, where applicable. | [optional] 
**agency_name** | **str** | The name of the public transit agency or sharing system provider which operates the service used during this leg, where applicable | [optional] 
**agency_time_zone_offset** | **float** | The difference between the local timezone and UTC at the time of the trip, represented in milliseconds. | [optional] 
**agency_url** | **str** | A URL providing information about the transit agency or sharing system provider | [optional] 
**distance** | **float** | Distance travelled in this leg, in meters | 
**duration** | **float** | Duration of travel in this leg, in seconds | 
**end_time** | **float** | Time at which this leg ends, represented in UNIX time in milliseconds. | 
**_from** | [**InlineResponse2002PlanFrom1**](InlineResponse2002PlanFrom1.md) |  | [optional] 
**headsign** | **str** | For transit legs, The text nominally displayed on the vehicle servicing the journey, which describes the route, destination, etc. See documentation for [GTFS trip_headsign](https://developers.google.com/transit/gtfs/reference#tripstxt) for further information. | [optional] 
**intermediate_stops** | [**list[InlineResponse2002PlanIntermediateStops]**](InlineResponse2002PlanIntermediateStops.md) | A list of all the stops the rider will pass through on this leg without embarking or disembarking the vehicle. | [optional] 
**leg_geometry** | [**InlineResponse2002PlanLegGeometry**](InlineResponse2002PlanLegGeometry.md) |  | [optional] 
**mode** | **str** | The mode used for this leg. A public transit mode will be denoted with a specific vehicle type if applicable (e.g. BUS) or TRANSIT for other types. | 
**route** | **str** | Alias of &#x60;routeShortName&#x60; | [optional] 
**route_color** | **str** | &#x60;route_color&#x60; from the [GTFS](https://developers.google.com/transit/gtfs/reference#routestxt) | [optional] 
**route_id** | **str** | Correlates with real-time data when available. | [optional] 
**global_route_id** | [**GlobalRouteId**](GlobalRouteId.md) |  | [optional] 
**route_long_name** | **str** | For transit legs, &#x60;route_long_name&#x60; from the [GTFS](https://developers.google.com/transit/gtfs/reference#routestxt) | [optional] 
**route_short_name** | **str** | For transit legs, &#x60;route_short_name&#x60; from the [GTFS](https://developers.google.com/transit/gtfs/reference#routestxt) | [optional] 
**route_text_color** | **str** | &#x60;route_text_color&#x60; from the [GTFS](https://developers.google.com/transit/gtfs/reference#routestxt) | [optional] 
**route_type** | **float** | For transit legs, &#x60;route_type&#x60; from the [GTFS](https://developers.google.com/transit/gtfs/reference#routestxt) | [optional] 
**start_time** | **float** | Time at which this leg begins, represented in UNIX time in milliseconds | 
**to** | [**InlineResponse2002PlanTo**](InlineResponse2002PlanTo.md) |  | [optional] 
**transit_leg** | **bool** | True if travelling by public transit, false for all other modes | [optional] 
**trip_block_id** | **str** | &#x60;block_id&#x60; from the [GTFS](https://developers.google.com/transit/gtfs/reference#tripstxt) if applicable for this leg. | [optional] 
**trip_id** | **str** | Correlates with real-time data where available | [optional] 
**trip_short_name** | **str** | &#x60;trip_short_name&#x60; from the [GTFS](https://developers.google.com/transit/gtfs/reference#tripstxt). Generally used for train numbers on commuter rail. | [optional] 
**trip_search_key** | [**TripSearchKey**](TripSearchKey.md) |  | [optional] 
**interline_with_previous_leg** | **bool** | For transit legs. True if an in-seat transfer / trip continuation has occured. The rider may remain onboard while the vehicle now operates on another route. | [optional] 
**real_time** | **bool** | True if startTime and endTime for this leg are based on real-time information. | [optional] 
**departure_delay** | **float** | Delay between the scheduled and real-time startTimes, in seconds. If positive, the vehicle is late, and if negative, the vehicle is early. | [optional] 
**arrival_delay** | **float** | Delay between the scheduled and real-time arrival of the vehicle. This value is currently always identical to departureDelay | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

