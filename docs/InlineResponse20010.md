# InlineResponse20010

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**route** | [**Route**](Route.md) |  | 
**rt_trip_id** | **str** | A identifier for that trip. In the majority of cases, it will be the same &#x60;trip_id&#x60; found from the original GTFS. Note that two departures can have the same &#x60;rt_trip_id&#x60; if one of them has been duplicated by real time data. Can be empty string if no trip id is provided.  | 
**schedule_items** | [**list[InlineResponse20010ScheduleItems]**](InlineResponse20010ScheduleItems.md) | The departure time and information about each stop of the trip. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

