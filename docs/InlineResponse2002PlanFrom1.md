# InlineResponse2002PlanFrom1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **float** |  | 
**lon** | **float** |  | 
**name** | **str** | The name of the transit stop ([GTFS stop_name](https://developers.google.com/transit/gtfs/reference#stopstxt)), or other point | [optional] 
**stop_code** | **str** | For transit stops, &#x60;stop_code&#x60; from the [GTFS](https://developers.google.com/transit/gtfs/reference#stopstxt) | [optional] 
**stop_id** | **str** | Correlates with real-time data when available | [optional] 
**global_stop_id** | [**GlobalStopId**](GlobalStopId.md) |  | [optional] 
**stop_index** | **float** | For transit stops: Represents the order of the stop within the GTFS trip. For example, stopIndex &#x3D; 2 means that the bus was boarded at the 3rd stop of the trip. | [optional] 
**vertex_type** | [**VertexType**](VertexType.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

