# InlineResponse2009Results

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**global_stop_id** | [**GlobalStopId**](GlobalStopId.md) |  | 
**location_type** | [**LocationType**](LocationType.md) |  | 
**match_strength** | **float** | Higher numbers indicate better matches for the search term | 
**parent_station_global_stop_id** | [**GlobalStopId**](GlobalStopId.md) |  | 
**route_type** | [**RouteType**](RouteType.md) |  | 
**stop_lat** | **float** | Latitude of the stop | 
**stop_lon** | **float** | Longitude of the stop | [default to 0]
**stop_name** | **str** | Stop name from the [GTFS](https://github.com/google/transit/blob/master/gtfs/spec/en/reference.md#stopstxt) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

